# Recipe App API Proxy

NGINX Proxy app for our recipe app API

## Usage

### Environment Variables

- `LISTEN_PORT` - Port to list on (default: `8000`)
- `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
- `APP_PORT` - Port of the app to forward requests t0 (default `9000`)
